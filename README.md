# README #

Requires requests and (eventually) random to be installed. 
However, most standard installs come with both but if not...install them!

Version 0.1 - only returns a list of the first headline and the subreddit it came from i.e. ['headline', 'subreddit'].

Usage:
RandomHeadline() returns the random headline and subreddit by calling 'https://www.reddit.com/r/random.json' You can substitute a specific subreddit.json to only get the headline from that one. 

Features: 
Outputs a formatted string so that the i2c display doesn't retain characters from a previous screen while scrolling side to side. 
Some error catching since the reddit api doesn't like being called often.

Bugs: 
KeyError when json response isn't a subreddit but a 429 error due to too many calls. 

To-Do:
merge with 16x2 scrolling and add driver library
random article selection - add to the random by picking a random article off the page
more error checking
combine some functions since why call parts when I can call one (or maybe not; I could use other parts for stuff....hmmm)

