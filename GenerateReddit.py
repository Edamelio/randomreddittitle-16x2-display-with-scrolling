import requests
import random

def RandomRedditArticle(site):
    r = requests.get(site)
    reddit = r.json()
    artnum = random.randrange(10)
    data = []
    #check response code: reddit is really picky about how often you call a page
    if r.status_code == 200:
        pass
    else:
        RandomRedditArticle(site)
    #generage headline, subreddit
    for child in reddit['data']['children']:
        try:
            data.append(child['data']['title'])
            data.append(child['data']['subreddit'])
        except KeyError: #wish this would work...maybe i'm not doing it right? 
            RandomRedditArticle(site)
            
        return(data[0:2])

#formats output of RandomRedditArticle to be 16xx2 friendly
def cleanReddit(data):
    row = []
    if len(data[0]) > 16 and len(data[1]) < 16:
        row.append((data[0] + (" " * (16 - (len(data[0]) % 16)))))
        row.append(data[1])
        return(row)
    elif len(data[1]) > 16 and len(data[0]) < 16:
        row.append(data[0])
        row.append((data[1] + (" " * (16 - (len(data[1]) % 16)))))
        return(row)
    elif len(data[0]) > 16 and len(data[1]) > 16:
        row.append((data[0] + (" " * (16 - (len(data[0]) % 16)))))
        row.append((data[1] + (" " * (16 - (len(data[1]) % 16)))))
        return(row)
    else:
        row.append(data[0])
        row.append(data[1])
        return(row)
#Just a combo of previous functions; I probably don't need it...
def RandomHeadline(site='https://www.reddit.com/r/random.json'):
    print(cleanReddit(RandomRedditArticle(site)))